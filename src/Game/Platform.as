package Game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.masks.Pixelmask;
	import flash.display.BitmapData;
	
	/**
	 * ...
	 * @author 
	 */
	public class Platform extends Entity 
	{
		
		[Embed(source = 'platform.png')] private const IM:Class;
		
		internal var mouseX:Number, mouseY:Number;
		internal var Im:Image = new Image(IM);
		internal var mybitmap:BitmapData;
		
		public function Platform() 
		{
			x = FP.width / 2;
			y = FP.height / 2;
			Im.centerOO();
			graphic = Im;
			
			//mask = new Pixelmask(IM, Im.x - Im.width / 2, Im.y - Im.height / 2);
			
			type = "platform";
			
			var size:int = Math.ceil(Math.sqrt(Im.width * Im.width + Im.height * Im.height));
			mybitmap = new BitmapData(size, size, true, 0);
			mask = new Pixelmask(mybitmap, -size / 2, -size / 2);
			
			collidable = true;
		}
		
		override public function update():void
		{
			mouseX = Input.mouseX;
			mouseY = Input.mouseY;
			Im.angle = FP.angle(x, y, mouseX, mouseY) - 90;
			
			mybitmap.fillRect(mybitmap.rect, 0);
			FP.point.x = mybitmap.width / 2;
			FP.point.y = mybitmap.height / 2;
			Im.render(mybitmap, FP.point, FP.zero);

		}
		
	}

}