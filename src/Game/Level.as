package Game 
{
	import Box2D.Common.Math.b2Vec2;
	import net.box2fp.Box2DWorld
	import net.box2fp.MyContactListener;
	import net.flashpunk.Sfx;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author 
	 */
	public class Level extends Box2DWorld 
	{
		internal var asteroids:Object;
		
		internal var contact_listener:MyContactListener = new MyContactListener();
		[Embed(source="sounds.swf#gameloop")] private const GAMEMUSIC:Class;
		public var mus:Sfx = new Sfx(GAMEMUSIC);
		
		public function Level() 
		{
			trace("Level started!");
			setGravity(new b2Vec2(0, 0));
			//doDebug();
			mus.volume = 0.6;
			mus.play();
			mus.loop();
		}
		
		override public function begin():void
		{
			super.begin();
			add (new Planet);
			add (new Sky);
			//add (new Orbit);
			add (new Platformb2d);
			
			b2world.SetContactListener(contact_listener);
			
			//add (new Platform);
			//add (new Asteroid);
		}
		
		override public function update():void
		{
			super.update();
			
			if (Math.random() > 0.98)
			{
				var tx:Number;
				var ty:Number;
				var sel:Number = Math.random() * 4;
				sel = Math.round(sel);
				//trace (sel);
				if (sel == 1) { tx = 0; ty = Math.random() * FP.height; add (new Asteroidb2d(tx, ty, 1)); }
				if (sel == 2) { tx = Math.random() * FP.width; ty = 0; add (new Asteroidb2d(tx, ty, 2)); }
				if (sel == 3) { tx = FP.width; ty = Math.random() * FP.height; add (new Asteroidb2d(tx, ty, 3)); }
				if (sel == 4) { tx = Math.random() * FP.width; ty = FP.height; add (new Asteroidb2d(tx, ty, 4)); }
				
				//tx = Math.random() * FP.width - 20; 
				//ty = Math.random() * FP.height - 20;
				//add (new Asteroidb2d(tx, ty));
			}
		}
		
		override protected function get positionIterations():int
		{
			return 30;
		}
		
		override protected function get velocityIterations():int
		{
			return 30;
		}
		
	}

}