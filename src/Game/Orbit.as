package Game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author 
	 */
	public class Orbit extends Entity 
	{
		
		[Embed(source = 'orbit.png')] private const IM:Class;
		
		public function Orbit() 
		{
			x = FP.width / 2;
			y = FP.height / 2;
			var Im:Image = new Image(IM);
			Im.centerOO();
			graphic = Im;
		}
		
	}

}