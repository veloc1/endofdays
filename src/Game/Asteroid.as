package Game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.masks.Pixelmask;
	
	/**
	 * ...
	 * @author 
	 */
	public class Asteroid extends Entity 
	{
		
		[Embed(source = 'aster.png')] private const IM:Class;
		
		internal var Im:Image = new Image(IM);
		
		internal var dir_x:Number = 3, dir_y:Number = 3;
		
		public function Asteroid() 
		{
			x = 0;
			y = 30;
			Im.centerOO();
			graphic = Im;
			mask = new Pixelmask(IM, Im.x - Im.width / 2, Im.y - Im.height / 2);
			
			type = "asteroid";
		}
		
		override public function update():void
		{
			x += dir_x;
			y += dir_y;
			if (collide("platform", x, y)) 
			{
				var mass1:Number = 16;
				var mass2:Number = 88;
				var velX1:Number = dir_x;
				var velX2:Number = 0;
				var velY1:Number = dir_y;
				var velY2:Number = 0;
				
				var c_x:Number = (x * mass2 + FP.halfWidth * mass1) / (mass1 + mass2);
				var c_y:Number = (y * mass2 + FP.halfHeight * mass1) / (mass1 + mass2);
				var c_dist:Number = Math.sqrt(Math.pow(FP.halfWidth - c_x, 2) + Math.pow(FP.halfHeight - c_y, 2));
				var n_x:Number = (FP.halfWidth - c_x) / c_dist;
				var n_y:Number = (FP.halfHeight - c_y) / c_dist;
				var p:Number = 2 * (velX1 * n_x + velY1 * n_y) / (mass1 + mass2);
				
				var newVelX:Number = velX1 - p * mass1 * n_x;// - p * mass2 * n_x;
				var newVelY:Number = velY1 - p * mass1 * n_x;// - p * mass2 * n_x;
				dir_x = newVelX;
				dir_y = newVelY;
				trace (dir_x, dir_y);
				
			}
			
			if (collide("asteroid", x, y))
			{
				
			}
			
			if (collide("earth", x, y)) 
			{
				FP.log("Game Over!"); 
				destroy(); 
			}
			
			if (x < -30) { destroy(); }
			if (x > FP.width + 30) { destroy(); }
			if (y < -30) { destroy(); }
			if (x > FP.height + 30) { destroy(); }
		}
		
		public function destroy():void
		{
			FP.world.remove(this);
		}
		
	}

}