package Game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author 
	 */
	public class Sky extends Entity 
	{
		[Embed(source = 'sky.png')]
		private const SKYIM:Class;
		
		public var sprSky:Spritemap = new Spritemap(SKYIM, 128, 126);
		
		public function Sky() 
		{
			sprSky.centerOO();
			x = FP.width / 2;
			y = FP.height / 2;
			sprSky.add("stay", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 7, true);
			graphic = sprSky;
			sprSky.play("stay");
			sprSky.angle = 20;
		}
		
		override public function update():void
		{
			
		}
		
	}

}