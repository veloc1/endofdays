package Game
{
	import Box2D.Common.Math.b2Vec2;
	import net.box2fp.Box2DEntity;
	import Box2D.Dynamics.b2Body;
	import net.box2fp.Box2DShapeBuilder;
	import net.box2fp.Box2DUtils;
	import net.box2fp.graphics.SuperGraphiclist;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.World;
	
	/**
	 * ...
	 * @author
	 */
	public class Asteroidb2d extends Box2DEntity
	{
		
		[Embed(source='aster.png')]
		private static const IM:Class;
		internal var side:Number;
		
		public function Asteroidb2d(x:Number, y:Number, an:Number)
		{
			super(x, y, 24, 24, b2Body.b2_dynamicBody);
			var Im:Image = new Image(IM);
			Im.centerOrigin();
			Im.x = 14;
			Im.y = 14;
			side = an;
			(graphic as SuperGraphiclist).add(Im);
		}
		
		override public function buildShapes(friction:Number, density:Number, restitution:Number, group:int, category:int, collmask:int):void
		{
			Box2DShapeBuilder.buildCircle(body, width / (2.0 * box2dworld.scale), 0, 0, 1);
		}
		
		override public function added():void
		{
			super.added();
			//var n_angle:Number = FP.angle(x, y, FP.halfWidth, FP.halfHeight) + 90;
			//trace (x, y, n_angle);
			//var n_v:Number = Math.random() * 5;
			body.SetAngularVelocity(Math.random() * 5 - 2.5);
			//body.SetLinearVelocity(new b2Vec2(n_v * Math.sin(n_angle), n_v * Math.cos(n_angle)));
			if (side == 1)
			{
				body.SetLinearVelocity(new b2Vec2(Math.random() * 5, Math.random() * 5 - 2.5));
			}
			if (side == 2)
			{
				body.SetLinearVelocity(new b2Vec2(Math.random() * 5 - 2.5, Math.random() * 5));
			}
			if (side == 3)
			{
				body.SetLinearVelocity(new b2Vec2(Math.random() * 5 - 5, Math.random() * 5 - 2.5));
			}
			if (side == 4)
			{
				body.SetLinearVelocity(new b2Vec2(Math.random() * 5 - 2.5, Math.random() * 5 - 5));
			}
		}
		
		public function destroy():void
		{
			FP.world.remove(this);
		}
		
		override public function update():void
		{
			super.update()
			if (x < -30)
			{
				destroy();
			}
			if (x > FP.width + 30)
			{
				destroy();
			}
			if (y < -30)
			{
				destroy();
			}
			if (y > FP.height + 30)
			{
				destroy();
			}
		}
	
	}

}