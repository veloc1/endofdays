package Game
{
	import net.box2fp.Box2DEntity;
	import net.box2fp.Box2DUtils;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import net.box2fp.Box2DShapeBuilder;
	import net.box2fp.graphics.SuperGraphiclist;
	
	/**
	 * ...
	 * @author
	 */
	public class Platformb2d extends Box2DEntity
	{
		
		[Embed(source='platform.png')]
		private const IM:Class;
		
		internal var mouseX:Number, mouseY:Number;
		internal var Im:Image = new Image(IM);
		
		public function Platformb2d()
		{
			super(FP.halfWidth - 45, FP.halfHeight - 6, 90, 10, b2Body.b2_kinematicBody);
			Im.centerOrigin();
			Im.x = 45;
			Im.y = 4;
			(graphic as SuperGraphiclist).add(Im);
		}
		
		override public function update():void
		{
			super.update();
			mouseX = Input.mouseX;
			mouseY = Input.mouseY;
			this.angle = -FP.angle(FP.halfWidth, FP.halfHeight, mouseX, mouseY) + 90;
		}
		
		override public function buildShapes(friction:Number, density:Number, restitution:Number, group:int, category:int, collmask:int):void
		{
			Box2DShapeBuilder.buildPoly(body, 0, 0, 0);
			//Box2DShapeBuilder.buildRectangle(body, width / (2.0 * box2dworld.scale), height / (2.0 * box2dworld.scale));
		}
	}

}