package Game 
{
	import Box2D.Collision.b2AABB;
	import Box2D.Dynamics.b2Body;
	import net.box2fp.Box2DEntity;
	import net.box2fp.Box2DShapeBuilder;
	import net.box2fp.graphics.SuperGraphiclist;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.masks.Pixelmask;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author 
	 */
	public class Planet extends Box2DEntity
	{
		[Embed(source = 'earth.png')]
		private const PLANETIM:Class;
		[Embed(source = 'earths.png')]
		private const IM:Class;
		[Embed(source = "sounds.swf#expl")] private const EXPL:Class;
		
		public var Expl:Sfx = new Sfx(EXPL);
		
		public var sprEarth:Spritemap = new Spritemap(PLANETIM, 128, 126);
		
		public function Planet() 
		{
			super(FP.width / 2 - 130 / 2, FP.height / 2 - 130 / 2, 130, 130, b2Body.b2_dynamicBody);
			sprEarth.centerOO();
			sprEarth.x = 130 / 2;
			sprEarth.y = 130 / 2;
			//x = FP.width / 2;
			//y = FP.height / 2;
			sprEarth.add("stay", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 5, true);
			//graphic = sprEarth;
			(graphic as SuperGraphiclist).add(sprEarth);
			sprEarth.play("stay");
			sprEarth.angle = 20;
			
			type = "earth";
		}
		
		override public function buildShapes(friction:Number, density:Number, restitution:Number, group:int, category:int, collmask:int):void
		{
			Box2DShapeBuilder.buildCircleSensor(body, width / (2.0 * box2dworld.scale), 0, 1, 0);
		}
		
		override public function update():void
		{
			super.update();
			
			if (body.GetUserData() == "GameOver")
			{
				FP.log("GameOver!");
				Expl.play();
				(FP.world as Level).mus.stop()
				body.SetUserData("AGameOver");
				FP.world = new GameOverScreen;
			}
		}
		
	}

}