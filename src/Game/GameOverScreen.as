package Game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Input;
	import net.flashpunk.World;
	
	/**
	 * ...
	 * @author 
	 */
	public class GameOverScreen extends World 
	{
		
		public function GameOverScreen() 
		{
			var text:Text = new Text("GameOver!");
			text.size = 42;
			var textEnt:Entity = new Entity(0, 0, text);
			textEnt.x = (FP.width/2)-(text.width/2);
			textEnt.y = (FP.height/2)-(text.height/2);
			add(textEnt);
			var text2:Text = new Text("Click to restart!");
			var textEnt2:Entity = new Entity(0, 0, text2);
			textEnt2.x = (FP.width/2)-(text2.width/2);
			textEnt2.y = (FP.height/2)-(text2.height/2)+200;
			add(textEnt2);
		}
		
		override public function update():void
		{
			if (Input.mouseDown) { FP.world = new Level; }
		}
		
	}

}