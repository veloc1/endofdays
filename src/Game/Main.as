package Game
{

	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	
	//[SWF(width = "640", height = "480", backgroundColor = "#000000")]
	[Frame(factoryClass = "Game.Preloader")]
	
	public class Main extends Engine
	{
		public function Main()
		{
			super(640, 480, 30, false);
			
		}
		
		override public function init():void
		{
			trace("Engine started!");

			FP.world = new Level;
			
			//FP.console.enable();
			FP.log("The game has started!");
		}
		
	}
}