package net.box2fp 
{
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.b2Fixture;
	
	/**
	 * ...
	 * @author 
	 */
	public class MyContactListener extends b2ContactListener 
	{
		
		public function MyContactListener() 
		{
			
		}
		
		override public function BeginContact(contact:b2Contact):void
		{
			// getting the fixtures that collided
			var fixtureA:b2Fixture=contact.GetFixtureA();
			var fixtureB:b2Fixture=contact.GetFixtureB();
			// if the fixture is a sensor, mark the parent body to be removed
			if (fixtureB.IsSensor()) {
				if (fixtureB.GetBody().GetUserData() != "AGameOver")
				fixtureB.GetBody().SetUserData("GameOver");
			}
			if (fixtureA.IsSensor()) {
				if (fixtureA.GetBody().GetUserData() != "AGameOver")
				fixtureA.GetBody().SetUserData("GameOver");
			}
		}
		
	}

}